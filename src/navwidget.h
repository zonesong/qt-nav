﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QList>
#include <QVBoxLayout>
#include <QStackedWidget>

#define MAIN_RIGHT_WIDGET_NUM 4
// ----- Nav color -----
#define BUTTON_CLEAR_COLOR "QPushButton{background-color:rgb(220,220,220);border:3px solid rgb(30,144,255);}"
#define BUTTON_SELECTED_COLOR "QPushButton{background-color: rgb(0,0,255);color: rgb(255,255,255);border:3px solid rgb(30,144,255);};"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    void initMainLeft(QWidget *widget);

private:
    Ui::Widget *ui;

public:
    static QStackedWidget *mainRight;
    // Navigation corresponding area
    static QList<QWidget *> mainRightWidgetList;
};

#endif // WIDGET_H
