﻿#include "navwidget.h"
#include "ui_widget.h"
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTabWidget>
#include <QPushButton>
#include <QList>
#include <QStackedWidget>

// ------------ global variable

QStackedWidget *Widget::mainRight = NULL;
QList<QWidget *> Widget::mainRightWidgetList;

// ------------ nav

class NavButtonList : public QWidget
{
public:
    NavButtonList(uint length, QWidget *parent = nullptr) : QWidget(parent)
    {
        this->length = length;
        for (uint i = 0; i < length; i++) {
            QPushButton *one = new QPushButton(this);
            this->buttonList.append(one);
            connect(one, &QPushButton::pressed, this, &NavButtonList::changeButtonColor);
        }

    }
    ~NavButtonList()
    {
        qDeleteAll(buttonList);
    }

    QPushButton *index(uint i)
    {
        return buttonList.value(i);
    }

private slots:
    void changeButtonColor()
    {
        QPushButton *button = qobject_cast<QPushButton*>(sender());
        if (button) {
            button->setStyleSheet(BUTTON_SELECTED_COLOR);
            this->selected = buttonList.indexOf(button);
            Widget::mainRight->setCurrentIndex(this->selected);
        }

        for (int i = 0; i < buttonList.count(); i++) {
            if ((uint)i != this->selected) {
                buttonList.value(i)->setStyleSheet(BUTTON_CLEAR_COLOR);
            }
        }
    }
private:
    QList<QPushButton *> buttonList;
    uint length;
    uint selected;
};

// ------------

void Widget::initMainLeft(QWidget *widget)
{
    QVBoxLayout *layout = new QVBoxLayout(widget);

    QWidget *top = new QWidget(widget);
    top->setWindowState(Qt::WindowMaximized);
    top->setStyleSheet(".QWidget{background-color:rgb(0,0,255);border:3px solid rgb(30,144,255)}");

    NavButtonList *bottom = new NavButtonList(MAIN_RIGHT_WIDGET_NUM, widget);
    bottom->setWindowState(Qt::WindowMaximized);
    bottom->setStyleSheet(".QWidget{background-color:rgb(244,244,244);border:3px solid rgb(30,144,255)}");

    bottom->index(0)->setWindowState(Qt::WindowMaximized);
    bottom->index(0)->setStyleSheet(BUTTON_CLEAR_COLOR);
    bottom->index(0)->setText(QString::fromLocal8Bit("唐三藏师傅"));
    bottom->index(0)->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    bottom->index(1)->setWindowState(Qt::WindowMaximized);
    bottom->index(1)->setStyleSheet(BUTTON_CLEAR_COLOR);
    bottom->index(1)->setText(QString::fromLocal8Bit("孙悟空大师兄"));
    bottom->index(1)->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    bottom->index(2)->setWindowState(Qt::WindowMaximized);
    bottom->index(2)->setStyleSheet(BUTTON_CLEAR_COLOR);
    bottom->index(2)->setText(QString::fromLocal8Bit("猪八戒二哥"));
    bottom->index(2)->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    bottom->index(3)->setWindowState(Qt::WindowMaximized);
    bottom->index(3)->setStyleSheet(BUTTON_CLEAR_COLOR);
    bottom->index(3)->setText(QString::fromLocal8Bit("沙僧沙和尚"));
    bottom->index(3)->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    QVBoxLayout *layoutBottom = new QVBoxLayout(bottom);
    layoutBottom->addWidget(bottom->index(0), 1);
    layoutBottom->addWidget(bottom->index(1), 1);
    layoutBottom->addWidget(bottom->index(2), 1);
    layoutBottom->addWidget(bottom->index(3), 1);
    layoutBottom->addStretch(8);

    layout->addWidget(top, 1);
    layout->addWidget(bottom, 6);
}

static void initArea2(QWidget *widget)
{
   QHBoxLayout *layout = new QHBoxLayout(widget);

   QWidget *area1 = new QWidget(widget);
   area1->setWindowState(Qt::WindowMaximized);
   area1->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");

   QWidget *area2 = new QWidget(widget);
   area2->setWindowState(Qt::WindowMaximized);
   area2->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");

   QWidget *area3 = new QWidget(widget);
   area3->setWindowState(Qt::WindowMaximized);
   area3->setStyleSheet(".QWidget{background-color:rgb(255,255,255);border:3px solid rgb(30,144,255)}");

   QWidget *area4 = new QWidget(widget);
   area4->setWindowState(Qt::WindowMaximized);
   area4->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");

   layout->addWidget(area1, 30);
   layout->addWidget(area2, 35);
   layout->addWidget(area3, 5);
   layout->addWidget(area4, 35);
}

static QWidget *initMainRight1(QWidget * widget)
{
    QWidget *area = new QWidget(widget);
    area->setWindowState(Qt::WindowMaximized);
//    QVBoxLayout *layoutArea = new QVBoxLayout(widget);
//    layoutArea->addWidget(area);

    QVBoxLayout *layout = new QVBoxLayout(area);

    QWidget *area1 = new QWidget(area);
    area1->setWindowState(Qt::WindowMaximized);
    area1->setStyleSheet(".QWidget{background-color:rgb(0,0,255);border:3px solid rgb(30,144,255)}");

    QWidget *area2 = new QWidget(area);
    area2->setWindowState(Qt::WindowMaximized);
    area2->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");
    initArea2(area2);

    QWidget *area3 = new QWidget(area);
    area3->setWindowState(Qt::WindowMaximized);
    area3->setStyleSheet(".QWidget{background-color:rgb(255,255,255);border:3px solid rgb(30,144,255)}");

    QWidget *area4 = new QWidget(area);
    area4->setWindowState(Qt::WindowMaximized);
    area4->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");

    layout->addWidget(area1, 10);
    layout->addWidget(area2, 45);
    layout->addWidget(area3, 5);
    layout->addWidget(area4, 40);

    return area;
}

static QWidget *initMainRight2(QWidget *widget)
{
    QWidget *area = new QWidget(widget);
    area->setWindowState(Qt::WindowMaximized);
//    QVBoxLayout *layoutArea = new QVBoxLayout(widget);
//    layoutArea->addWidget(area);

    QVBoxLayout *layout = new QVBoxLayout(area);

    QWidget *area1 = new QWidget(area);
    area1->setWindowState(Qt::WindowMaximized);
    area1->setStyleSheet(".QWidget{background-color:rgb(0,255,0);}");

    QWidget *area2 = new QWidget(area);
    area2->setWindowState(Qt::WindowMaximized);
    area2->setStyleSheet("QWidget{background-color:rgb(250,240,230);}");
    initArea2(area2);

    QWidget *area3 = new QWidget(area);
    area3->setWindowState(Qt::WindowMaximized);
    area3->setStyleSheet("Widget{background-color:rgb(255,255,255);}");

    QWidget *area4 = new QWidget(area);
    area4->setWindowState(Qt::WindowMaximized);
    area4->setStyleSheet("Widget{background-color:rgb(250,240,230);}");

    layout->addWidget(area1, 10);
    layout->addWidget(area2, 45);
    layout->addWidget(area3, 5);
    layout->addWidget(area4, 40);

    return area;
}

static QWidget *initMainRight3(QWidget *widget)
{
    QWidget *area = new QWidget(widget);
    area->setWindowState(Qt::WindowMaximized);
//    QVBoxLayout *layoutArea = new QVBoxLayout(widget);
//    layoutArea->addWidget(area);

    QVBoxLayout *layout = new QVBoxLayout(area);

    QWidget *area1 = new QWidget(area);
    area1->setWindowState(Qt::WindowMaximized);
    area1->setStyleSheet(".QWidget{background-color:rgb(255,0,0);border:3px solid rgb(30,144,255)}");

    QWidget *area2 = new QWidget(area);
    area2->setWindowState(Qt::WindowMaximized);
    area2->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");
    initArea2(area2);

    QWidget *area3 = new QWidget(area);
    area3->setWindowState(Qt::WindowMaximized);
    area3->setStyleSheet(".QWidget{background-color:rgb(255,255,255);border:3px solid rgb(30,144,255)}");

    QWidget *area4 = new QWidget(area);
    area4->setWindowState(Qt::WindowMaximized);
    area4->setStyleSheet(".QWidget{background-color:rgb(250,240,230);border:3px solid rgb(30,144,255)}");

    layout->addWidget(area1, 10);
    layout->addWidget(area2, 45);
    layout->addWidget(area3, 5);
    layout->addWidget(area4, 40);

    return area;
}

static QWidget *initMainRight4(QWidget *widget)
{
    QWidget *area = new QWidget(widget);
    area->setWindowState(Qt::WindowMaximized);
//    QVBoxLayout *layoutArea = new QVBoxLayout(widget);
//    layoutArea->addWidget(area);

    QVBoxLayout *layout = new QVBoxLayout(area);

    QWidget *area1 = new QWidget(area);
    area1->setWindowState(Qt::WindowMaximized);
    area1->setStyleSheet(".QWidget{background-color:rgb(0,255,255);}");

    QWidget *area2 = new QWidget(area);
    area2->setWindowState(Qt::WindowMaximized);
    area2->setStyleSheet("QWidget{background-color:rgb(250,240,230);}");
    initArea2(area2);

    QWidget *area3 = new QWidget(area);
    area3->setWindowState(Qt::WindowMaximized);
    area3->setStyleSheet("Widget{background-color:rgb(255,255,255);}");

    QWidget *area4 = new QWidget(area);
    area4->setWindowState(Qt::WindowMaximized);
    area4->setStyleSheet("Widget{background-color:rgb(250,240,230);}");

    layout->addWidget(area1, 10);
    layout->addWidget(area2, 45);
    layout->addWidget(area3, 5);
    layout->addWidget(area4, 40);

    return area;
}

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    this->setWindowState(Qt::WindowMaximized);

    QHBoxLayout *layoutMain = new QHBoxLayout(this);

    QWidget *mainLeft = new QWidget(this);
    mainLeft->setWindowState(Qt::WindowMaximized);
    mainLeft->setStyleSheet(".QWidget{background-color:rgb(244,244,244);border:3px solid rgb(30,144,255)}");
    initMainLeft(mainLeft);

    Widget::mainRight = new QStackedWidget(this);
    Widget::mainRight->setWindowState(Qt::WindowMaximized);
    Widget::mainRight->setStyleSheet(".QWidget{background-color:rgb(255,255,255);border:3px solid rgb(30,144,255)}");

    Widget::mainRight->addWidget(initMainRight1(mainRight));
    Widget::mainRight->addWidget(initMainRight2(mainRight));
    Widget::mainRight->addWidget(initMainRight3(mainRight));
    Widget::mainRight->addWidget(initMainRight4(mainRight));

    Widget::mainRight->setCurrentIndex(0);
//    Widget::mainRightWidgetList.append(initMainRight2(mainRight));
//    Widget::mainRightWidgetList.append(initMainRight1(mainRight));
//    Widget::mainRightWidgetList.append(initMainRight3(mainRight));
//    Widget::mainRightWidgetList.append(initMainRight4(mainRight));

//    for (int i = 0; i < Widget::mainRightWidgetList.count(); i++) {
//        Widget::mainRightWidgetList.value(i)->hide();
//    }
//    Widget::mainRightWidgetList.value(0)->show();

    layoutMain->addWidget(mainLeft, 1);
    layoutMain->addWidget(mainRight, 7);
}

Widget::~Widget()
{
    delete ui;
}
